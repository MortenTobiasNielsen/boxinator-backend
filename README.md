# Boxinator Backend

The backend for a company packing and sending packages.

Backend deployment link (Swagger documentation): https://boxinatorwebapi.azurewebsites.net/index.html

Database was designed by Entity Framework (code first approach)

All endpoints are subjects of rate limiting policy. Current rule is to allow max. 20 request per 10 seconds

On top of controllers, the system makes use of DTOs and services.


