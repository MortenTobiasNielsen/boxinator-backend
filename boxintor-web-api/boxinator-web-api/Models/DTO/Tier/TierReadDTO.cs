﻿namespace boxinator_web_api.Models {
    public class TierReadDTO {
        public int TierId { get; set; }
        public string Name { get; set; }
        public int Weight { get; set; }

    }
}
