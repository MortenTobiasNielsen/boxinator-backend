﻿namespace boxinator_web_api.Models {
    public class CountryCreateDTO {
        public string Name { get; set; }
        public int Multiplier { get; set; }
    }
}
