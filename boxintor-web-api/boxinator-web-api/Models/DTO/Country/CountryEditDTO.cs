﻿namespace boxinator_web_api.Models {
    public class CountryEditDTO {
        public int CountryId { get; set; }
        public string Name { get; set; }
        public int Multiplier { get; set; }
    }
}
