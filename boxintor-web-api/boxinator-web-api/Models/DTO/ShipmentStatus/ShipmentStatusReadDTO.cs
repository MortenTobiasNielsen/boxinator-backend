﻿namespace boxinator_web_api.Models {
    public class ShipmentStatusReadDTO {
        public int ShipmentStatusId { get; set; }
        public string Status { get; set; }
    }
}
