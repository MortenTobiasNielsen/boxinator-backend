﻿using System;

namespace boxinator_web_api.Models {
    public class ShipmentStatusHistoryReadDTO {
        public DateTime StatusChangeDateTime { get; set; }
        public int ShipmentStatusId { get; set; }
    }
}
