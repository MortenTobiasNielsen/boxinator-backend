﻿namespace boxinator_web_api.Models {
    public class ShipmentReadDTO {
        public int ShipmentId { get; set; }
        public string ReceiverName { get; set; }
        public string HexColor { get; set; }
        public decimal Price { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public int TierId { get; set; }
        public int ShipmentStatusId { get; set; }
    }
}
