﻿namespace boxinator_web_api.Models {
    public class ShipmentEditDTO {
        public int ShipmentId { get; set; }
        public int ShipmentStatusId { get; set; }
    }
}
