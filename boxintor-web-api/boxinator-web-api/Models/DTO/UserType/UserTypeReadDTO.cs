﻿namespace boxinator_web_api.Models {
    public class UserTypeReadDTO {
        public int UserTypeId { get; set; }
        public string Type { get; set; }
    }
}
