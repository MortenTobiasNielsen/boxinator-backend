﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace boxinator_web_api.Models
{
    public class ShipmentStatus
    {
        // PK
        public int ShipmentStatusId { get; set; }

        // Fields
        [Required]
        [MaxLength(9)]
        public string Status { get; set; }

        // Relationships
        public ICollection<Shipment>? Shipments { get; set; }
    }
}
