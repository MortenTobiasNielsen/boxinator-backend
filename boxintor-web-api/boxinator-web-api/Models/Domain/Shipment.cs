﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace boxinator_web_api.Models
{
    public class Shipment
    {
        // PK
        public int ShipmentId { get; set; }

        // Fields
        [Required]
        [MaxLength(50)]
        public string ReceiverName { get; set; }
        [Required]
        [MaxLength(8)]
        public string HexColor { get; set; }
        [Required]
        [Column(TypeName = "money")]
        public decimal Price { get; set; }
        [Required]
        [MaxLength(100)]
        public string Address { get; set; }
        [Required]
        [MaxLength(50)]
        public string Country { get; set; }

        // Relationships
        public int TierId { get; set; }
        public Tier Tier { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int ShipmentStatusId { get; set; }
        public ShipmentStatus ShipmentStatus { get; set; }
    }
}
