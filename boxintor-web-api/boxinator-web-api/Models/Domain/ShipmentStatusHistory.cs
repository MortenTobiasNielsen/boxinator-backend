﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace boxinator_web_api.Models
{
    public class ShipmentStatusHistory
    {
        // PK
        public int ShipmentStatusHistoryId { get; set; }

        // Fields
        [Required]
        public DateTime StatusChangeDateTime { get; set; }

        // Relationships
        public int ShipmentId { get; set; }
        public Shipment Shipment { get; set; }
        public int ShipmentStatusId { get; set; }
        public ShipmentStatus ShipmentStatus { get; set; }
    }
}
