﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace boxinator_web_api.Models {
    public class UserInfo {
        [ForeignKey("User")]
        // PK
        public int UserInfoId { get; set; }

        // Fields
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        [MaxLength(20)]
        public string PhoneNumber { get; set; }
        [MaxLength(200)]
        public string Address { get; set; }

        // Relationships
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public virtual User User { get; set; }
    }
}
