﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace boxinator_web_api.Models
{
    public class Tier
    {
        // PK
        public int TierId { get; set; }

        // Fields
        [Required]
        [MaxLength(9)]
        public string Name { get; set; }
        [Required]
        public int Weight { get; set; }

        // Relationships
        public ICollection<Shipment>? Shipments { get; set; }
    }
}
