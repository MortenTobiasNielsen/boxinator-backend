﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace boxinator_web_api.Models
{
    public class UserType
    {
        // PK
        public int UserTypeId { get; set; }

        // Fields
        [Required]
        [MaxLength(15)]
        public string Type { get; set; }

        // Relationships
        public ICollection<User>? Users { get; set; }
    }
}
