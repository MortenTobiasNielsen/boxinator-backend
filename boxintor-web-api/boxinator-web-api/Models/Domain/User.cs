﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace boxinator_web_api.Models
{
    public class User
    {
        // PK
        public int UserId { get; set; }

        // Fields
        [Required]
        [MaxLength(50)]
        public string Email { get; set; }

        //Relationships
        public int UserTypeId { get; set; }
        public UserType UserType { get; set; }
        public virtual UserInfo UserInfo { get; set; }
        public ICollection<Shipment>? Shipments { get; set; }
    }
}
