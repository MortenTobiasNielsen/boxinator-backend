﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace boxinator_web_api.Models
{
    public class BoxinatorDbContext : DbContext
    {
        public DbSet<ShipmentStatus> ShipmentStatuses { get; set; }
        public DbSet<UserInfo> UserInfos { get; set; }
        public DbSet<Tier> Tiers { get; set; }
        public DbSet<UserType> UserTypes { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Shipment> Shipments { get; set; }
        public DbSet<ShipmentStatusHistory> ShipmentStatusHistories { get; set; }
        public DbSet<User> Users { get; set; }
        
        

        public BoxinatorDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //if(!optionsBuilder.IsConfigured)
            //{
            //optionsBuilder.UseSqlServer("Server=tcp:boxinator2021.database.windows.net,1433;Initial Catalog=boxinatorSV;Persist Security Info=False;User ID=boxAdmin;Password=123qwe!@#;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            //}
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seed data
            modelBuilder.Entity<Country>().HasData(new Country { CountryId = 1, Name = "Denmark", Multiplier = 1 });
            modelBuilder.Entity<Country>().HasData(new Country { CountryId = 2, Name = "Sweden", Multiplier = 1  });
            modelBuilder.Entity<Country>().HasData(new Country { CountryId = 3, Name = "Norway", Multiplier = 1  });

            modelBuilder.Entity<UserType>().HasData(new UserType { UserTypeId = 1, Type="GUEST" });
            modelBuilder.Entity<UserType>().HasData(new UserType { UserTypeId = 2, Type = "REGISTERED_USER" });
            modelBuilder.Entity<UserType>().HasData(new UserType { UserTypeId = 3, Type = "ADMINISTRATOR" });

            modelBuilder.Entity<ShipmentStatus>().HasData(new ShipmentStatus { ShipmentStatusId = 1, Status = "CREATED" });
            modelBuilder.Entity<ShipmentStatus>().HasData(new ShipmentStatus { ShipmentStatusId = 2, Status = "RECEIVED" });
            modelBuilder.Entity<ShipmentStatus>().HasData(new ShipmentStatus { ShipmentStatusId = 3, Status = "INTRANSIT" });
            modelBuilder.Entity<ShipmentStatus>().HasData(new ShipmentStatus { ShipmentStatusId = 4, Status = "COMPLETED" });
            modelBuilder.Entity<ShipmentStatus>().HasData(new ShipmentStatus { ShipmentStatusId = 5, Status = "CANCELLED" });

            modelBuilder.Entity<Tier>().HasData(new Tier { TierId = 1, Name = "Basic", Weight = 1 });
            modelBuilder.Entity<Tier>().HasData(new Tier { TierId = 2, Name = "Humble", Weight = 2 });
            modelBuilder.Entity<Tier>().HasData(new Tier { TierId = 3, Name = "Deluxe", Weight = 5 });
            modelBuilder.Entity<Tier>().HasData(new Tier { TierId = 4, Name = "Premium", Weight = 8 });

            //modelBuilder.Entity<UserInfo>().HasData(new UserInfo { UserInfoId = 1, FirstName = "Dominik", LastName = "Drat", DateOfBirth = new DateTime(1992, 6, 23), PhoneNumber = "51505050", Address = "Nice Street 5, 2750 Ballerup", CountryId = 1 });
            //modelBuilder.Entity<UserInfo>().HasData(new UserInfo { UserInfoId = 2, FirstName = "Dominik", LastName = "Drat", DateOfBirth = new DateTime(1993, 7, 24), PhoneNumber = "52505050", Address = "Nice Street 4, 2750 Ballerup", CountryId = 2 });
            //modelBuilder.Entity<UserInfo>().HasData(new UserInfo { UserInfoId = 3, FirstName = "Dominik", LastName = "Drat", DateOfBirth = new DateTime(1994, 8, 25), PhoneNumber = "53505050", Address = "Nice Street 3, 2750 Ballerup", CountryId = 3 });
            //modelBuilder.Entity<UserInfo>().HasData(new UserInfo { UserInfoId = 4, FirstName = "test", LastName = "test", DateOfBirth = new DateTime(1992, 6, 23), PhoneNumber = "51505050", Address = "Nice Street 5, 2750 Ballerup", CountryId = 1 });
            //modelBuilder.Entity<UserInfo>().HasData(new UserInfo { UserInfoId = 5, FirstName = "test", LastName = "test", DateOfBirth = new DateTime(1993, 7, 24), PhoneNumber = "52505050", Address = "Nice Street 4, 2750 Ballerup", CountryId = 2 });
            //modelBuilder.Entity<UserInfo>().HasData(new UserInfo { UserInfoId = 6, FirstName = "test", LastName = "test", DateOfBirth = new DateTime(1994, 8, 25), PhoneNumber = "53505050", Address = "Nice Street 3, 2750 Ballerup", CountryId = 3 });

            //modelBuilder.Entity<User>().HasData(new User { UserId = 1, Email = "dominik.admin@email.com", UserInfoId = 1, UserTypeId = 3 });
            //modelBuilder.Entity<User>().HasData(new User { UserId = 2, Email = "dominik.user@email.com", UserInfoId = 2, UserTypeId = 2 });
            //modelBuilder.Entity<User>().HasData(new User { UserId = 3, Email = "dominik.guest@email.com", UserInfoId = 3, UserTypeId = 1 });
            //modelBuilder.Entity<User>().HasData(new User { UserId = 4, Email = "test.admin@email.com", UserInfoId = 4, UserTypeId = 3 });
            //modelBuilder.Entity<User>().HasData(new User { UserId = 5, Email = "test.user@email.com", UserInfoId = 5, UserTypeId = 2 });
            //modelBuilder.Entity<User>().HasData(new User { UserId = 6, Email = "test.guest@email.com", UserInfoId = 6, UserTypeId = 1 });

            //modelBuilder.Entity<Shipment>().HasData(new Shipment { ShipmentId = 1, ReceiverName = "ReceiverName 1", HexColor = "FFFFFF00", TierId = 1, UserId = 1, ShipmentStatusId = 1, Address = "Test Address1", Country = "Denmark", Price = 200 });
            //modelBuilder.Entity<Shipment>().HasData(new Shipment { ShipmentId = 2, ReceiverName = "ReceiverName 2", HexColor = "FFAAFF00", TierId = 2, UserId = 2, ShipmentStatusId = 1, Address = "Test Address2", Country = "Denmark", Price = 200 });
            //modelBuilder.Entity<Shipment>().HasData(new Shipment { ShipmentId = 3, ReceiverName = "ReceiverName 3", HexColor = "FFBBFF00", TierId = 3, UserId = 2, ShipmentStatusId = 2, Address = "Test Address1", Country = "Denmark", Price = 200 });
            //modelBuilder.Entity<Shipment>().HasData(new Shipment { ShipmentId = 4, ReceiverName = "ReceiverName 4", HexColor = "FFCCFF00", TierId = 4, UserId = 3, ShipmentStatusId = 2, Address = "Test Address2", Country = "Denmark", Price = 200 });

            //modelBuilder.Entity<ShipmentStatusHistory>().HasData(new ShipmentStatusHistory { ShipmentStatusHistoryId = 1, ShipmentId = 1, ShipmentStatusId = 1, StatusChangeDateTime = new DateTime(2021, 10, 12) });
            //modelBuilder.Entity<ShipmentStatusHistory>().HasData(new ShipmentStatusHistory { ShipmentStatusHistoryId = 2, ShipmentId = 2, ShipmentStatusId = 1, StatusChangeDateTime = new DateTime(2021, 10, 12) });
            //modelBuilder.Entity<ShipmentStatusHistory>().HasData(new ShipmentStatusHistory { ShipmentStatusHistoryId = 3, ShipmentId = 3, ShipmentStatusId = 1, StatusChangeDateTime = new DateTime(2021, 10, 12) });
            //modelBuilder.Entity<ShipmentStatusHistory>().HasData(new ShipmentStatusHistory { ShipmentStatusHistoryId = 4, ShipmentId = 3, ShipmentStatusId = 2, StatusChangeDateTime = new DateTime(2021, 10, 12) });
            //modelBuilder.Entity<ShipmentStatusHistory>().HasData(new ShipmentStatusHistory { ShipmentStatusHistoryId = 5, ShipmentId = 4, ShipmentStatusId = 1, StatusChangeDateTime = new DateTime(2021, 10, 12) });
            //modelBuilder.Entity<ShipmentStatusHistory>().HasData(new ShipmentStatusHistory { ShipmentStatusHistoryId = 6, ShipmentId = 4, ShipmentStatusId = 2, StatusChangeDateTime = new DateTime(2021, 10, 12) });

        }
    }
}
