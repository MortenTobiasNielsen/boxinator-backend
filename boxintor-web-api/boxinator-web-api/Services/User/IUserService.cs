﻿using boxinator_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace boxinator_web_api.Services
{
    public interface IUserService
    {
        public Task<IEnumerable<UserInfo>> GetUsersAsync();
        public Task<User> GetUserByEmailAsync(string email);
        public Task<UserInfo> GetUserInfoById(int userId);
        public Task<User> GetUserByIdAsync(int userId);
        public Task UpdateUserAsync(User user);
        public Task AddUserInfoAsync(UserInfo userInfo);
        public Task UpdateUserInfoAsync(UserInfo userInfo);
        public Task CreateUserAsync(User user);
        public Task RemoveUserAsync(int id);
        public bool UserExists(int userId);
        public Task DetachUser(User user);
    }
}
