﻿using boxinator_web_api.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace boxinator_web_api.Services
{
    public class UserService : IUserService
    {
        private readonly BoxinatorDbContext _context;

        public UserService(BoxinatorDbContext context)
        {
            _context = context;
        }

        public async Task<User> GetUserByEmailAsync(string email)
        {
            return _context.Users.SingleOrDefault(user => user.Email == email);
        }

        public async Task<User> GetUserByIdAsync(int userId)
        {
            return await _context.Users.FindAsync(userId);
        }

        public async Task<UserInfo> GetUserInfoById(int userId)
        {
            return await _context.UserInfos.FindAsync(userId);
        }

        public async Task<IEnumerable<UserInfo>> GetUsersAsync()
        {
            return await _context.UserInfos.ToListAsync();
        }

        public async Task UpdateUserAsync(User user)
        {
            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task AddUserInfoAsync(UserInfo userInfo)
        {
            _context.UserInfos.Add(userInfo);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateUserInfoAsync(UserInfo userInfo)
        {
            _context.Entry(userInfo).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task CreateUserAsync(User user)
        {
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
        }

        public bool UserExists(int userId)
        {
            return _context.Users.Any(e => e.UserId == userId);
        }

        public async Task RemoveUserAsync(int id)
        {
            var user = await GetUserByIdAsync(id);
            _context.Users.Remove(user);

            var userInfo = await GetUserInfoById(id);
            if (userInfo != null)
            {
                _context.UserInfos.Remove(userInfo);
            }

            await _context.SaveChangesAsync();
        }
        public async Task DetachUser(User user)
        {
            _context.Entry(user).State = EntityState.Detached;
            await _context.SaveChangesAsync();
        }
    }
}
