﻿using boxinator_web_api.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace boxinator_web_api.Services
{
    public class ShipmentService : IShipmentService
    {
        private readonly BoxinatorDbContext _context;

        public ShipmentService(BoxinatorDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Shipment>> GetActiveShipmentsAsync(int userId, int userTypeId)
        {
            
            if (userTypeId == 2)
            {
                return await _context.Shipments.Where(s => s.ShipmentStatusId < 4 && s.UserId == userId).ToListAsync();
            }

            return await _context.Shipments.Where(s => s.ShipmentStatusId < 4).ToListAsync();
        }

        public async Task<IEnumerable<Shipment>> GetCancelledShipmentsAsync(int userId, int userTypeId)
        {
            if (userTypeId == 2)
            {
                return await _context.Shipments.Where(s => s.ShipmentStatusId == 5 && s.UserId == userId).ToListAsync();
            }
            return await _context.Shipments.Where(s => s.ShipmentStatusId == 5).ToListAsync();
        }

        public async Task<IEnumerable<Shipment>> GetCompletedShipmentsAsync(int userId, int userTypeId)
        {
            if (userTypeId == 2)
            {
                return await _context.Shipments.Where(s => s.ShipmentStatusId == 4 && s.UserId == userId).ToListAsync();
            }
            return await _context.Shipments.Where(s => s.ShipmentStatusId == 4).ToListAsync();
        }

        public async Task<List<Shipment>> GetCustomerShipmentsAsync(int id)
        {
            return await _context.Shipments.Where(s => s.UserId == id).ToListAsync();
        }

        public async Task<Shipment> GetShipmentByIdAsync(int id)
        {
            return await _context.Shipments.FindAsync(id);
        }

        public async Task<List<ShipmentStatusHistory>> GetShipmentStatusHistoriesAsync(int id)
        {
            return await _context.ShipmentStatusHistories.Where(h => h.ShipmentId == id).ToListAsync();
        }

        public async Task UpdateShipmentAsync(Shipment shipment)
        {
            _context.Entry(shipment).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public async Task AddShipmentAsync(Shipment shipment)
        {
            _context.Shipments.Add(shipment);
            await _context.SaveChangesAsync();

        }

        public async Task RemoveShipmentAsync(int id)
        {
            // It is required to remove shipment entries in history table
            var shipmentHistories = await GetShipmentStatusHistoriesAsync(id);
            foreach (var item in shipmentHistories)
            {
                _context.ShipmentStatusHistories.Remove(item);
            }

            var shipment = await GetShipmentByIdAsync(id);
            _context.Shipments.Remove(shipment);
            await _context.SaveChangesAsync();
        }

        public bool ShipmentExists(int id)
        {
            return _context.Shipments.Any(e => e.ShipmentId == id);
        }

        public async Task AddStatusHistoryAsync(Shipment shipment)
        {
            ShipmentStatusHistory newShipmentHistory = new() { ShipmentId = shipment.ShipmentId, ShipmentStatusId = shipment.ShipmentStatusId, StatusChangeDateTime = System.DateTime.Now };
            _context.ShipmentStatusHistories.Add(newShipmentHistory);
            await _context.SaveChangesAsync();
        }
    }
}
