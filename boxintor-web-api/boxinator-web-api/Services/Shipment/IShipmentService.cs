﻿using boxinator_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace boxinator_web_api.Services
{
    public interface IShipmentService
    {
        public Task<IEnumerable<Shipment>> GetActiveShipmentsAsync(int userId, int userTypeId);
        public Task<IEnumerable<Shipment>> GetCompletedShipmentsAsync(int userId, int userTypeId);
        public Task<IEnumerable<Shipment>> GetCancelledShipmentsAsync(int userId, int userTypeId);
        public Task<Shipment> GetShipmentByIdAsync(int id);
        public Task<List<ShipmentStatusHistory>> GetShipmentStatusHistoriesAsync(int id);
        public Task<List<Shipment>> GetCustomerShipmentsAsync(int id);
        public Task UpdateShipmentAsync(Shipment shipment);
        public Task AddShipmentAsync(Shipment shipment);
        public Task RemoveShipmentAsync(int id);

        public Task AddStatusHistoryAsync(Shipment shipment);
        public bool ShipmentExists(int id);
    }
}
