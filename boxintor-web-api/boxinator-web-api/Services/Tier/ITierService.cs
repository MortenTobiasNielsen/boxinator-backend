﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using boxinator_web_api.Models;

namespace boxinator_web_api.Services
{
    public interface ITierService
    {
        public Task<IEnumerable<Tier>> GetTiersAsync();
    }
}
