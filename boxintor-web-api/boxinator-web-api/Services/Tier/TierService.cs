﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using boxinator_web_api.Models;
using Microsoft.EntityFrameworkCore;

namespace boxinator_web_api.Services
{
    public class TierService : ITierService
    {
        private readonly BoxinatorDbContext _context;
        public TierService(BoxinatorDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Tier>> GetTiersAsync()
        {
            return await _context.Tiers.ToListAsync();
        }
    }
}
