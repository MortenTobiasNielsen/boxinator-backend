﻿using boxinator_web_api.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace boxinator_web_api.Services
{
    public class ShipmentStatusService : IShipmentStatusService
    {
        private readonly BoxinatorDbContext _context;
        public ShipmentStatusService(BoxinatorDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<ShipmentStatus>> GetShipmentStatusesAsync()
        {
            return await _context.ShipmentStatuses.ToListAsync();
        }
    }
}
