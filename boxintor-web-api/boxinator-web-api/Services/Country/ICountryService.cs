﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using boxinator_web_api.Models;


namespace boxinator_web_api.Services
{
    public interface ICountryService
    {
        public Task<IEnumerable<Country>> GetCountriesAsync();
        public Task<Country> GetCountryAsync(int id);
        public Task CreateCountryAsync(Country country);
        public Task UpdateCountryAsync(Country country);
        public Task<Country> GetCountryByNameAsync(string name);
        public Task DetachCountry(Country country);
        public bool CountryExists(int id);
    }
}
