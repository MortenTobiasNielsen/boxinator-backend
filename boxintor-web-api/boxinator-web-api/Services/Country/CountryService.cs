﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using boxinator_web_api.Models;
using Microsoft.EntityFrameworkCore;

namespace boxinator_web_api.Services
{
    public class CountryService : ICountryService
    {
        private readonly BoxinatorDbContext _context;

        public CountryService(BoxinatorDbContext context)
        {
            _context = context;
        }

        public bool CountryExists(int id)
        {
            return _context.Countries.Any(country => country.CountryId == id);
        }

        public async Task CreateCountryAsync(Country country)
        {
            _context.Countries.Add(country);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Country>> GetCountriesAsync()
        {
            return await _context.Countries.ToListAsync();
        }

        public async Task<Country> GetCountryAsync(int id)
        {
                return await _context.Countries.FindAsync(id);
        }

        public async Task<Country> GetCountryByNameAsync(string name)
        {
            return _context.Countries.Where(c => c.Name == name).SingleOrDefault();
        }

        public async Task DetachCountry(Country country)
        {
            _context.Entry(country).State = EntityState.Detached;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateCountryAsync(Country country)
        {
            _context.Entry(country).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
