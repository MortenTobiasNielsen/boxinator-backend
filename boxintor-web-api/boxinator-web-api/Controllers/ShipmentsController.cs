﻿using AutoMapper;
using boxinator_web_api.Models;
using boxinator_web_api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;

namespace boxinator_web_api.Controllers {
    [Route("api/v1/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class ShipmentsController : ControllerBase {
        private readonly IMapper _mapper;
        private readonly IShipmentService _shipmentService;
        private readonly IUserService _userService;

        private const int _flatFee = 200;

        public ShipmentsController(IUserService userService, IShipmentService shipmentService, IMapper mapper) {
            _mapper = mapper;
            _shipmentService = shipmentService;
            _userService = userService;
        }

        /// <summary>
        /// Gets the flat fee of sending a package
        /// </summary>
        /// <returns>The flat fee of sending a package</returns>
        [HttpGet("FlatFee")]
        public ActionResult<int> GetFlatFee() {
            return _flatFee;
        }

        /// <summary>
        /// Gets all active shipments 
        /// </summary>
        /// <returns>A list of all active shipments in the database</returns>
        [HttpGet("Active/{email}")]
        public async Task<ActionResult<IEnumerable<ShipmentReadDTO>>> GetActiveShipments(string email) {
            User tempUser = await _userService.GetUserByEmailAsync(email);
            if (tempUser == null || tempUser.UserTypeId == 1) {
                return BadRequest();
            }
            return _mapper.Map<List<ShipmentReadDTO>>(await _shipmentService.GetActiveShipmentsAsync(tempUser.UserId, tempUser.UserTypeId));
        }

        /// <summary>
        /// Gets all completed shipments 
        /// </summary>
        /// <returns>A list of all completed shipments in the database</returns>
        [HttpGet("Completed/{email}")]
        public async Task<ActionResult<IEnumerable<ShipmentReadDTO>>> GetCompletedShipments(string email) {
            User tempUser = await _userService.GetUserByEmailAsync(email);
            if (tempUser == null || tempUser.UserTypeId == 1) {
                return BadRequest();
            }
            return _mapper.Map<List<ShipmentReadDTO>>(await _shipmentService.GetCompletedShipmentsAsync(tempUser.UserId, tempUser.UserTypeId));
        }

        /// <summary>
        /// Gets all cancelled shipments 
        /// </summary>
        /// <returns>A list of all cancelled shipments in the database</returns>
        [HttpGet("Cancelled/{email}")]
        public async Task<ActionResult<IEnumerable<ShipmentReadDTO>>> GetCancelledShipments(string email) {
            User tempUser = await _userService.GetUserByEmailAsync(email);
            if (tempUser == null || tempUser.UserTypeId == 1) {
                return BadRequest();
            }
            return _mapper.Map<List<ShipmentReadDTO>>(await _shipmentService.GetCancelledShipmentsAsync(tempUser.UserId, tempUser.UserTypeId));
        }

        /// <summary>
        /// Gets a specific shipment by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns the specified shipment if it exists</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ShipmentReadDTO>> GetShipment(int id) {
            Shipment shipment = await _shipmentService.GetShipmentByIdAsync(id);

            if (shipment == null) {
                return NotFound();
            }

            return _mapper.Map<ShipmentReadDTO>(shipment);
        }

        /// <summary>
        /// Gets the shipment status history of a specific shipment by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns the shipment status history of a specified shipment if it exists</returns>
        [HttpGet("History/{id}")]
        public async Task<ActionResult<List<ShipmentStatusHistoryReadDTO>>> GetShipmentStatusHistories(int id) {
            List<ShipmentStatusHistory> shipmentStatusHistory = await _shipmentService.GetShipmentStatusHistoriesAsync(id);

            if (shipmentStatusHistory.Count == 0) {
                return NotFound($"No shipment history found for shipment ID: {id}");
            }

            return _mapper.Map<List<ShipmentStatusHistoryReadDTO>>(shipmentStatusHistory);
        }

        /// <summary>
        /// Gets all shipments of a specified customer id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of all shipments for a specific customer</returns>
        [HttpGet("Customer/{id}")]
        public async Task<ActionResult<List<ShipmentReadDTO>>> GetCustomerShipments(int id) {
            List<Shipment> shipments = await _shipmentService.GetCustomerShipmentsAsync(id);

            return _mapper.Map<List<ShipmentReadDTO>>(shipments);
        }

        /// <summary>
        /// Updates the shipment status of a specific shipment based on an id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="shipment"></param>
        /// <returns>An Http response code</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateShipment(int id, ShipmentEditDTO shipment) {
            if (id != shipment.ShipmentId) {
                return BadRequest();
            }

            if (!_shipmentService.ShipmentExists(id)) {
                return NotFound();
            }

            Shipment domainShipment = await _shipmentService.GetShipmentByIdAsync(id);

            int newShipmentStatusId = shipment.ShipmentStatusId;

            if (domainShipment.ShipmentStatusId == newShipmentStatusId) {
                return BadRequest();
            }

            domainShipment.ShipmentStatusId = newShipmentStatusId;

            await _shipmentService.UpdateShipmentAsync(domainShipment);
            await _shipmentService.AddStatusHistoryAsync(domainShipment);

            return NoContent();
        }

        /// <summary>
        /// Adds a new shipment.
        /// </summary>
        /// <param name="shipment"></param>
        /// <returns>An Http response code</returns>
        [HttpPost]
        public async Task<ActionResult<Shipment>> CreateShipment(ShipmentCreateDTO shipment) {
            Shipment domainShipment = _mapper.Map<Shipment>(shipment);
            domainShipment.ShipmentStatusId = 1;
            domainShipment.User = null;

            User domainUser = await _userService.GetUserByEmailAsync(shipment.UserEmail);

            if (domainUser == null) {
                domainUser = new User() {
                    Email = shipment.UserEmail,
                    UserTypeId = 1,
                    UserType = null,
                    Shipments = null,
                    UserInfo = null,
                };
                await _userService.CreateUserAsync(domainUser);
                domainUser = await _userService.GetUserByEmailAsync(shipment.UserEmail);
            }

            domainShipment.UserId = domainUser.UserId;
            UserInfo domainUserInfo = await _userService.GetUserInfoById(domainUser.UserId);

            await _shipmentService.AddShipmentAsync(domainShipment);
            await _shipmentService.AddStatusHistoryAsync(domainShipment);

            string greeting = "Dear customer";

            if (domainUserInfo != null) {
                greeting = @$"Dear {domainUserInfo.FirstName}";
            }

            try {
                var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
                var config = builder.Build();

                var smtpClient = new SmtpClient(config["Smtp:Host"]) {
                    Port = int.Parse(config["Smtp:Port"]),
                    Credentials = new NetworkCredential(config["Smtp:Username"], config["Smtp:Password"]),
                    EnableSsl = true,
                };

                var mailMessage = new MailMessage {
                    From = new MailAddress(config["Smtp:Username"]),
                    Subject = "Package order received",
                    // TODO: Should we try to get the customers name, so we can write it instead of customer?
                    // TODO: Remember to change the link to the frontend deployed website
                    Body = @$"
                        <p>{greeting}</p>
                        <p>
                          Thank you for your order. The shipment has been registered and the receiver
                          will have it within 2-3 business days.
                        </p>
                        <p>
                          You can see the shipment history of this and any past orders you have made by
                          <a href='https://boxinator-ddmn-dk.herokuapp.com/register'>registering</a>.
                        </p>
                        <p> Best regards <br/> The Boxinator Team </p>",
                    IsBodyHtml = true,
                };
                mailMessage.To.Add(domainUser.Email);

                smtpClient.Send(mailMessage);
            } catch (DbUpdateConcurrencyException) {
                throw;
            }

            return CreatedAtAction("GetShipment", new { id = domainShipment.ShipmentId }, _mapper.Map<ShipmentReadDTO>(domainShipment));
        }

        /// <summary>
        /// Deletes a specified shipment by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An Http response code</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteShipment(int id) {
            Shipment shipment = await _shipmentService.GetShipmentByIdAsync(id);
            if (shipment == null) {
                return NotFound();
            }

            await _shipmentService.RemoveShipmentAsync(id);

            return Ok();
        }
    }
}
