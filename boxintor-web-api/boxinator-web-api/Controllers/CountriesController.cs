﻿using AutoMapper;
using boxinator_web_api.Models;
using boxinator_web_api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace boxinator_web_api.Controllers {
    [Route("api/v1/Settings/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CountriesController : ControllerBase {
        private readonly IMapper _mapper;
        private readonly ICountryService _countryService;

        public CountriesController(IMapper mapper, ICountryService countryService)
        {
            _mapper = mapper;
            _countryService = countryService;
        }

        /// <summary>
        /// Gets all the countries with their associated cost multiplier
        /// </summary>
        /// <returns>A list of all the countries in the database with their associated cost multiplier</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CountryReadDTO>>> GetCountries() {
            return _mapper.Map<List<CountryReadDTO>>(await _countryService.GetCountriesAsync());
        }

        /// <summary>
        /// Gets a specific country by id with the associated cost multiplier
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns the specified country, if it exists, with the associated cost multiplier</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CountryReadDTO>> GetCountry(int id) {
            Country country = await _countryService.GetCountryAsync(id);

            if (country == null) {
                return NotFound();
            }

            return _mapper.Map<CountryReadDTO>(country);
        }

        /// <summary>
        /// Updates a specific country based on an id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="country"></param>
        /// <returns>An Http response code</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCountry(int id, CountryEditDTO country) {
            if (id != country.CountryId) {
                return BadRequest();
            }

            if (!_countryService.CountryExists(id)) {
                return NotFound();
            }

            Country countryInDB = await _countryService.GetCountryByNameAsync(country.Name);
            if (countryInDB != null)
            {
                if (countryInDB.CountryId != country.CountryId)
                {
                    return BadRequest();
                }

                await _countryService.DetachCountry(countryInDB);
            }

            countryInDB = _mapper.Map<Country>(country);
            
            await _countryService.UpdateCountryAsync(countryInDB);

            return NoContent();
        }

        /// <summary>
        /// Adds a new country.
        /// </summary>
        /// <param name="country"></param>
        /// <returns>An Http response code</returns>
        [HttpPost]
        public async Task<ActionResult<CountryReadDTO>> CreateCountry(CountryCreateDTO country) {
            Country domainCountry = await _countryService.GetCountryByNameAsync(country.Name);
            if (domainCountry != null) {
                return BadRequest($"Country already exists: Use a put request with {domainCountry.CountryId} as the id to change the cost multiplier");
            }

            domainCountry = _mapper.Map<Country>(country);
            await _countryService.CreateCountryAsync(domainCountry);

            return CreatedAtAction("GetCountry", new { id = domainCountry.CountryId }, _mapper.Map<CountryReadDTO>(domainCountry));
        }
    }
}
