﻿using AutoMapper;
using boxinator_web_api.Models;
using boxinator_web_api.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;

namespace boxinator_web_api.Controllers {
    [Route("api/v1/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class UsersController : ControllerBase {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public UsersController(IUserService userService, IMapper mapper) {
            _mapper = mapper;
            _userService = userService;
        }

        // TODO: Should this be possible - is it stated somewhere that the admin should have access to a list of all users?
        /// <summary>
        /// Gets all the users
        /// </summary>
        /// <returns>A list of all the users in the database</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetUsers() {
            return _mapper.Map<List<UserReadDTO>>(await _userService.GetUsersAsync());
        }

        /// <summary>
        /// Gets a specific user by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns>Returns user information if the user is a registered user, and "Guest user" as the success message if it is a guest user</returns>
        [HttpGet("email/{email}")]
        public async Task<ActionResult<UserReadDTO>> GetUserByEmail(string email) {
            User domainUser = await _userService.GetUserByEmailAsync(email);

            if (domainUser == null) {
                return NotFound();
            }

            UserInfo domainUserInfo = await _userService.GetUserInfoById(domainUser.UserId);

            if (domainUserInfo == null) {
                return Ok("Guest user");
            }

            return _mapper.Map<UserReadDTO>(domainUserInfo);
        }


        /// <summary>
        /// Updates a specific user based on an id. Guests are changed to registered users.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns>An Http response code</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateUser(int id, UserEditDTO user) {
            if (id != user.UserId) {
                return BadRequest();
            }

            if (!_userService.UserExists(id)) {
                return NotFound();
            }

            User domainUser = await _userService.GetUserByIdAsync(id);
            UserInfo domainUserInfo = _mapper.Map<UserInfo>(user);

            if (domainUser.UserTypeId == 1) {
                domainUser.UserTypeId = 2;

                await _userService.UpdateUserAsync(domainUser);
                await _userService.AddUserInfoAsync(domainUserInfo);

            } else {
                if (domainUser.Email != user.Email) {
                    domainUser.Email = user.Email;
                    await _userService.UpdateUserAsync(domainUser);
                }
                await _userService.UpdateUserInfoAsync(domainUserInfo);
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new user. The user type will be Guest if no first name is provided, and otherwise registered_user.
        /// </summary>
        /// <param name="user"></param>
        /// <returns>An Http response code</returns>
        [HttpPost]
        public async Task<IActionResult> CreateUser(UserCreateDTO user) {

            User domainUser = await _userService.GetUserByEmailAsync(user.Email);
            if (domainUser != null) {
                if (domainUser.UserTypeId == 2) {
                    return BadRequest("A user with that email already exists - please log in.");
                } else if (domainUser.UserTypeId == 1) {

                    await _userService.DetachUser(domainUser);
                    User updatedUser = _mapper.Map<User>(user);
                    updatedUser.UserId = domainUser.UserId;
                    updatedUser.UserTypeId = 2;

                    await _userService.UpdateUserAsync(updatedUser);

                    UserInfo domainUserInfo = _mapper.Map<UserInfo>(user);
                    domainUserInfo.UserInfoId = updatedUser.UserId;

                    await _userService.AddUserInfoAsync(domainUserInfo);
                }
            } else {
                domainUser = _mapper.Map<User>(user);
                domainUser.UserTypeId = 2;
                await _userService.CreateUserAsync(domainUser);

                UserInfo domainUserInfo = _mapper.Map<UserInfo>(user);
                domainUserInfo.UserInfoId = domainUser.UserId;

                await _userService.AddUserInfoAsync(domainUserInfo);
            }
            return CreatedAtAction("GetUserByEmail", new { email = domainUser.Email }, _mapper.Map<UserReadDTO>(domainUser));
        }

        /// <summary>
        /// Deletes a specified user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An Http response code</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id) {
            User user = await _userService.GetUserByIdAsync(id);
            if (user == null) {
                return NotFound();
            }

            await _userService.RemoveUserAsync(id);

            return Ok();
        }
    }
}
