﻿using AutoMapper;
using boxinator_web_api.Models;
using boxinator_web_api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;

namespace boxinator_web_api.Controllers {
    [Route("api/v1/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class TiersController : ControllerBase {
        private readonly IMapper _mapper;
        private readonly ITierService _tierService;

        public TiersController(ITierService tierService, IMapper mapper) {
            _mapper = mapper;
            _tierService = tierService;
        }

        /// <summary>
        /// Gets all package tiers
        /// </summary>
        /// <returns>A list of all package tiers in the database</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TierReadDTO>>> GetTiers() {
            return _mapper.Map<List<TierReadDTO>>(await _tierService.GetTiersAsync());
        }
    }
}
