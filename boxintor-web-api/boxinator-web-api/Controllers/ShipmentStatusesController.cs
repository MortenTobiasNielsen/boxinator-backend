﻿using AutoMapper;
using boxinator_web_api.Models;
using boxinator_web_api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;

namespace boxinator_web_api.Controllers {
    [Route("api/v1/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class ShipmentStatusesController : ControllerBase {
        private readonly IMapper _mapper;
        private readonly IShipmentStatusService _shipmentStatusService;

        public ShipmentStatusesController(IShipmentStatusService shipmentStatusService, IMapper mapper) {
            _mapper = mapper;
            _shipmentStatusService = shipmentStatusService;
        }

        /// <summary>
        /// Gets all shipment statuses 
        /// </summary>
        /// <returns>A list of all shipment statuses in the database</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ShipmentStatusReadDTO>>> GetShipmentStatuses() {
            return _mapper.Map<List<ShipmentStatusReadDTO>>(await _shipmentStatusService.GetShipmentStatusesAsync());
        }
    }
}