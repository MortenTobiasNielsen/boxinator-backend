﻿using AutoMapper;
using boxinator_web_api.Models;

namespace boxinator_web_api.Profiles {
    public class UserTypeProfile : Profile {
        public UserTypeProfile() {
            CreateMap<UserType, UserTypeReadDTO>().ReverseMap();
        }
    }
}
