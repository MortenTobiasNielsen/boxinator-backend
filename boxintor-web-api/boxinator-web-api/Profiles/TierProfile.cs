﻿using AutoMapper;
using boxinator_web_api.Models;

namespace boxinator_web_api.Profiles {
    public class TierProfile : Profile{
        public TierProfile() {
            CreateMap<Tier, TierReadDTO>().ReverseMap();
        }
    }
}
