﻿using AutoMapper;
using boxinator_web_api.Models;

namespace boxinator_web_api.Profiles {
    public class ShipmentStatusHistoryProfile : Profile {
        public ShipmentStatusHistoryProfile() {
            CreateMap<ShipmentStatusHistory, ShipmentStatusHistoryReadDTO>().ReverseMap();
        }
    }
}
