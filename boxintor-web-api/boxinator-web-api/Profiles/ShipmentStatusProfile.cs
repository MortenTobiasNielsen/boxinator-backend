﻿using AutoMapper;
using boxinator_web_api.Models;

namespace boxinator_web_api.Profiles {
    public class ShipmentStatusProfile : Profile {
        public ShipmentStatusProfile() {
            CreateMap<ShipmentStatus, ShipmentStatusReadDTO>().ReverseMap();
        }
    }
}
