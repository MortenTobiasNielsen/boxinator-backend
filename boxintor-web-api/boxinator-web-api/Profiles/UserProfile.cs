﻿using AutoMapper;
using boxinator_web_api.Models;

namespace boxinator_web_api.Profiles {
    public class UserProfile : Profile {
        public UserProfile() {
            CreateMap<User, UserReadDTO>().ReverseMap();
            CreateMap<User, UserEditDTO>().ReverseMap();
            CreateMap<User, UserCreateDTO>().ReverseMap();
            CreateMap<UserInfo, UserCreateDTO>().ReverseMap();
            CreateMap<UserInfo, UserReadDTO>()
                .ForMember(userDTO => userDTO.UserId, opt => opt.MapFrom(userInfo => userInfo.UserInfoId))
                .ReverseMap()
                .ForPath(userInfo => userInfo.UserInfoId, opt => opt.MapFrom(userDTO => userDTO.UserId));
            CreateMap<UserInfo, UserEditDTO>()
                .ForMember(userDTO => userDTO.UserId, opt => opt.MapFrom(userInfo => userInfo.UserInfoId))
                .ReverseMap()
                .ForPath(userInfo => userInfo.UserInfoId, opt => opt.MapFrom(userDTO => userDTO.UserId)); ;
        }
    }
}
