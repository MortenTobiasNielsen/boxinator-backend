﻿using AutoMapper;
using boxinator_web_api.Models;

namespace boxinator_web_api.Profiles {
    public class CountryProfile : Profile {
        public CountryProfile() {
            CreateMap<Country, CountryReadDTO>().ReverseMap();
            CreateMap<Country, CountryEditDTO>().ReverseMap();
            CreateMap<Country, CountryCreateDTO>().ReverseMap();
        }
    }
}
