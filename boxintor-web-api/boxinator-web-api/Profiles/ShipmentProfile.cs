﻿using AutoMapper;
using boxinator_web_api.Models;

namespace boxinator_web_api.Profiles {
    public class ShipmentProfile : Profile {
        public ShipmentProfile() {
            CreateMap<Shipment, ShipmentReadDTO>().ReverseMap();
            CreateMap<Shipment, ShipmentEditDTO>().ReverseMap();
            CreateMap<Shipment, ShipmentCreateDTO>().ReverseMap();
        }
    }
}
